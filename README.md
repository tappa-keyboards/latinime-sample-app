# Buildable AOSP LatinIME

LatinIME is included as a git submodule in order to simplify synchronization with latest LatinIME upstream.

To include submodule when cloning, pass `--recurse-submodules`:

```
git clone --recurse-submodules git@bitbucket.org:tappa-keyboards/latinime-sample-app.git
```

To try different LatinIME branches, go to `LatinIME` directory and switch branches as usual.
Don't forget to apply the top commit "Make project buildable".
